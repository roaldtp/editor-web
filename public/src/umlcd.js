var graph = new joint.dia.Graph();

var paper = new joint.dia.Paper({
  el: document.getElementById("paper"),
  width: 1860,
  height: 805,
  gridSize: 10,
  drawGrid: true,

  background: {
    color: "rgba(255, 255, 255, 0.5)",
  },
  model: graph,
});

paper.on({
  "element:pointerdblclick": function (elementView) {},

  "link:mouseenter": function (linkView) {
    linkView.addTools(
      new joint.dia.ToolsView({
        tools: [
          new joint.linkTools.Vertices({ snapRadius: 0 }),
          new joint.linkTools.SourceArrowhead(),
          new joint.linkTools.TargetArrowhead(),
          new joint.linkTools.Remove({
            distance: 20,
          }),
        ],
      })
    );
  },
  "element:mouseenter": function (elementView) {
    var model = elementView.model;
    var bbox = model.getBBox();
    var ellipseRadius = 1 - Math.cos(g.toRad(45));
    var offset =
      model.attr(["pointers", "pointerShape"]) === "ellipse"
        ? {
            x: (-ellipseRadius * bbox.width) / 2,
            y: (ellipseRadius * bbox.height) / 2,
          }
        : { x: -3, y: 3 };

    elementView.addTools(
      new joint.dia.ToolsView({
        tools: [
          new joint.elementTools.Remove({
            useModelGeometry: true,
            y: "0%",
            x: "100%",
            offset: offset,
          }),
        ],
      })
    );
  },
  "cell:mouseleave": function (cellView) {
    cellView.removeTools();
  },
  "blank:pointerdown": function (evt, x, y) {
    var data = (evt.data = {});
    var cell;
    if (evt.shiftKey) {
      cell = new uml.Generalization({});
      cell.source({ x: x, y: y });
      cell.target({ x: x, y: y });
      cell.addTo(this.model);
      data.cell = cell;
    }
  },
  "blank:pointermove": function (evt, x, y) {
    var data = evt.data;
    var cell = data.cell;
    if (cell.isLink()) {
      cell.target({ x: x, y: y });
    } else {
      return;
    }
  },
  "blank:pointerup": function (evt) {},
});

var uml = joint.shapes.uml;
var classes;
var uml1 = joint.shapes.uml;
var uml2 = joint.shapes.uml;
var listaDeClases = [];
const arrModel = {
  modelo: [
    {
      class: "",
      tipoclase: "",
      atributos: [],
      metodos: [],
    },
  ],
};

function crearNuevo(nom, atributo, metodo) {
  const x = Math.random() * 330;
  const y = Math.random() * 220;
  const w = calcularMayor(atributo, nom, metodo) * 7;
  const u = cantidadEl(atributo, metodo) * 15 + 6;
  var rect = new uml.Class({
    position: { x: x, y: y },
    size: { width: w, height: u },
    name: nom,
    attributes: atributo,
    methods: metodo,
    attrs: {
      ".uml-class-name-rect": {
        fill: "#feb662",
        stroke: "#ffffff",
        "stroke-width": 0.5,
      },
      ".uml-class-attrs-rect": {
        fill: "#fdc886",
        stroke: "#fff",
        "stroke-width": 0.5,
      },
      ".uml-class-methods-rect": {
        fill: "#fdc886",
        stroke: "#fff",
        "stroke-width": 0.5,
      },
      ".uml-class-attrs-text": {
        ref: ".uml-class-attrs-rect",
        "ref-y": 0.5,
        "y-alignment": "middle",
      },
      ".uml-class-methods-text": {
        ref: ".uml-class-methods-rect",
        "ref-y": 0.5,
        "y-alignment": "middle",
      },
    },
  });
  rect.addTo(this.graph);

  const objeto = {
    class: nom,
    tipoclase: "normal",
    atributos: atributo,
    metodos: metodo,
  };

  arrModel.modelo.push(objeto);
  //arrModel.modelo.shift()
  //  console.log(arrModel)

  return rect;
}

function crearInterface(nom, atributo, metodo) {
  const x = Math.random() * 330;
  const y = Math.random() * 220;
  const w = calcularMayor(atributo, nom, metodo) * 7;
  const u = cantidadEl(atributo, metodo) * 15 + 6;

  var rect2 = new uml1.Interface({
    position: { x: x, y: y },
    size: { width: w, height: u },
    name: nom,
    attributes: atributo,
    methods: metodo,
    attrs: {
      ".uml-class-name-rect": {
        fill: "#feb662",
        stroke: "#ffffff",
        "stroke-width": 0.5,
      },
      ".uml-class-attrs-rect": {
        fill: "#fdc886",
        stroke: "#fff",
        "stroke-width": 0.5,
      },
      ".uml-class-methods-rect": {
        fill: "#fdc886",
        stroke: "#fff",
        "stroke-width": 0.5,
      },
      ".uml-class-attrs-text": {
        ref: ".uml-class-attrs-rect",
        "ref-y": 0.5,
        "y-alignment": "middle",
      },
      ".uml-class-methods-text": {
        ref: ".uml-class-methods-rect",
        "ref-y": 0.5,
        "y-alignment": "middle",
      },
    },
  });
  rect2.addTo(this.graph);
  const objeto = {
    class: nom,
    tipoclase: "Interface",
    atributos: atributo,
    metodos: metodo,
  };

  arrModel.modelo.push(objeto);
  return rect2;
}

function crearAbstracta(nom, atributo, metodo) {
  const x = Math.random() * 330;
  const y = Math.random() * 220;
  const w = calcularMayor(atributo, nom, metodo) * 7;
  const u = cantidadEl(atributo, metodo) * 15 + 6;

  var rect1 = new uml2.Abstract({
    position: { x: x, y: y },
    size: { width: w, height: u },
    name: nom,
    attributes: atributo,
    methods: metodo,
    attrs: {
      ".uml-class-name-rect": {
        fill: "#68ddd5",
        stroke: "#ffffff",
        "stroke-width": 0.5,
      },
      ".uml-class-attrs-rect": {
        fill: "#9687fe",
        stroke: "#fff",
        "stroke-width": 0.5,
      },
      ".uml-class-methods-rect": {
        fill: "#9687fe",
        stroke: "#fff",
        "stroke-width": 0.5,
      },
      ".uml-class-methods-text, .uml-class-attrs-text": {
        fill: "#fff",
      },
    },
  });
  rect1.addTo(this.graph);
  const objeto = {
    class: nom,
    tipoclase: "abstracta",
    atributos: atributo,
    metodos: metodo,
  };

  arrModel.modelo.push(objeto);
  return rect1;
}

function nuevaRelacion(origen, destino) {
  const link = new uml.Generalization({
    source: { origen },
    target: { destino },
  });
  link.addTo(this.graph);
}

function calcularMayor(lista1, cadena, lista2) {
  var x = 0;
  lista1.forEach((e) => {
    if (x < e.length) {
      x = e.length;
    }
  });
  lista2.forEach((e) => {
    if (x < e.length) {
      x = e.length;
    }
  });
  if (cadena.length > x) x = cadena.length;
  return x;
}

function convertirStringToLista(cadena) {
  var lista;
  lista = cadena.split(",");
  // console.log(lista);
  return lista;
}

function cantidadEl(lista1, lista2) {
  var x = lista2.length + lista1.length;
  return x;
}

//let guardarM = document.getElementById("guardar");

function guardar() {
  const model = arrModel;

  console.log(model);
  fetch("/model", {
    method: "POST",
    body: JSON.stringify(model),
    headers: {
      "Content-type": "application/json",
    },
  })
    .then((res) => res.json())
    .catch((err) => console.log(err));

  mostrarIds();
}

function mostrarIds() {
  fetch("/model")
    .then((res) => res.json())
    .then((data) => mostrarDatos(data))
    .catch(function (error) {
      console.log(error);
    });
}

const mostrarDatos = (dato) => {
  console.log(dato);
  let template = "";
  for (let i = 0; i < dato.length; i++) {
    template += `<li>${dato[i]._id}</li>`;
  }
  document.getElementById("user-model").innerHTML = template;
};

function borrar() {
  const id = document.getElementById("id-borrar").value;
  console.log(id);
  fetch(`/model/${id}`, {
    method: "DELETE",
  });
  id.Remove;
}

function obtener() {
  const id = document.getElementById("id-obtener").value;
  console.log(id);
  fetch(`/model/${id}`)
    .then((res) => res.json())
    .then((dato) => mostrarModelos(dato))
    .catch((err) => console.log(err));
}

function mostrarModelos(dato) {
 // console.log("el modelo es", dato.modelo);
  const datos = dato.modelo;
  for (let i = 1; i < datos.length; i++) {
    // console.log(datos[i])
    if (datos[i].tipoclase == "normal") {
      crearNuevo(datos[i].class,datos[i].atributos,datos[i].metodos)
    }else if(datos[i].tipoclase == "abstracta"){
      crearAbstracta(datos[i].class,datos[i].atributos,datos[i].metodos)
    }else{
      crearInterface(datos[i].class,datos[i].atributos,datos[i].metodos)
    }
  }
}

function actualizar(){
  const model =arrModel
  const id = document.getElementById("actualizar-model").value;
  fetch(`/model/${id}`,{
    method:'PUT',
    headers:{
      "Content-type": "application/json",
    },
    body:JSON.stringify(model)
  })
  .then(res => res.json())
  .then(res => console.log(res))
}