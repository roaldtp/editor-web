const mongoose = require("mongoose");

const Model = mongoose.model("diagrama", {
  modelo: [
    {
      class: { type: String },
      tipoclase: { type: String },
      atributos: [{ type: String }],
      metodos: [{ type: String }],
    },
  ],
});

module.exports = Model;
