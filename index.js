const express = require("express");
const mongoose = require("mongoose");
const Modelo = require("./diagrama.model");
const app = express();
const port = 3000;
app.use(express.json());
mongoose.connect(
  "mongodb+srv://ronald:pi31416@cluster0.tpddi.mongodb.net/diagrama?retryWrites=true&w=majority"
);

app.get("/model/:id", async (req, res) => {
  const { id } = req.params;
  const model = await Modelo.findOne({ _id: id });  
  if(model){
    res.status(200).send(model);
  }else{
    res.send("el modelo no existe")
  }
  console.log(model);
  
});

app.get("/model", async (req, res) => {
  const { body } = req;
  const model = await Modelo.find();
  res.status(200).send(model);
});

app.put("/model/:id", async (req, res) => {
  const { id } = req.params;
  const model = await Modelo.findOne({ _id: id });
  Object.assign(model, req.body);
  await model.save();
  res.sendStatus(204);
});

app.post("/model", async (req, res) => {
  const model = new Modelo(req.body)
  if(model!=null){
    const saveModel = await model.save()
    res.status(201).send(saveModel._id) 
  }else{
    res.send("modelo vacio")
  }
});

app.delete("/model/:id", async (req, res) => {
  const { id } = req.params;
  const model = await Modelo.findOne({ _id: id });
  if (model) {
    model.remove();
    res.status(204).send("modelo eliminado");
  } else {
    res.send("el modelo no existe");
  }
});

app.use(express.static("public"));
app.get("/", (req, res) => {
  console.log(__dirname);
  res.sendFile(`${__dirname}/index.html`);
});

app.get("*", (req, res) => {
  res.status(404).send("esta pagina no existe");
});
app.listen(port, () => {
  console.log("arancando la app");
});


